package com.rental.management.propertyservices.facade;

import com.rental.management.propertyservices.model.*;
import com.rental.management.propertyservices.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PropertyFacade {
    @Autowired
    private AnnouncementService announcementService;
    @Autowired
    private PropertyService propertyService;
    @Autowired
    private PriceService priceService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private RenterPropertyService renterPropertyService;


    public Announcement getAnnouncementById(Long id) {
        return announcementService.getById(id);
    }

    public List<Announcement> getAllAnnouncements() {
        return announcementService.getAll();
    }

    public Announcement saveAnnouncement(Announcement announcement) {
        return announcementService.save(announcement);
    }

    public void deleteAnnouncement(Long id) {
        announcementService.delete(id);
    }

    public Announcement updateAnnouncement(Announcement announcement) {
        return announcementService.update(announcement);
    }

    public List<Announcement> getAnnouncementsByUserId(Long userId) {
        return announcementService.getByUser(userId);
    }


    public List<Price> getAllPrices() {
        return priceService.getAll();
    }

    public Price getPriceById(Long id) {
        return priceService.getById(id);
    }

    public Price savePrice(Price price) {
        return priceService.save(price);
    }

    public void deletePrice(Long id) {
        priceService.delete(id);
    }

    public Price updatePrice(Price price) {
        return priceService.update(price);
    }


    public List<Address> getAllAddresses() {
        return addressService.getAll();
    }

    public Address getAddressById(Long id) {
        return addressService.getById(id);
    }

    public List<Address> getAddressesByCity(String city) {
        return addressService.getByCity(city);
    }

    public Address saveAddress(Address address) {
        return addressService.save(address);
    }

    public void deleteAddress(Long id) {
        addressService.delete(id);
    }

    public Address updateAddress(Address address) {
        return addressService.update(address);
    }


    public List<Property> getAllProperties() {
        return propertyService.getAll();
    }

    public Property getPropertyById(Long id) {
        return propertyService.getById(id);
    }

    public List<Property> getPropertiesByLandlordId(Long id) {
        return propertyService.getAllByLandlordId(id);
    }

    public Property saveProperty(Property property) {
        return propertyService.save(property);
    }

    public void deleteProperty(Long id) {
        propertyService.delete(id);
    }

    public Property updateProperty(Property property) {
        return propertyService.update(property);
    }

    public RenterProperty getRenterPropertyByRenterId(Long id) {
        return renterPropertyService.getByRenterId(id);
    }

    public RenterProperty getRenterPropertyById(Long id){
        return renterPropertyService.getById(id);
    }

    public List<RenterProperty> getAllRenterProperties() {
        return renterPropertyService.getAll();
    }

    public List<RenterProperty> getAllRenterPropertiesByPropertyId(Long id) {
        return renterPropertyService.getByPropertyId(id);
    }

    public RenterProperty saveRenterProperty(RenterProperty renterProperty){
        return renterPropertyService.save(renterProperty);
    }
    public void deleteRenterProperty(Long id){
        renterPropertyService.delete(id);
    }

}
