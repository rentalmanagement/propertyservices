package com.rental.management.propertyservices.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "addresses")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "address", nullable = false)
    private String address;
    @Column(name = "county", nullable = false)
    private String county;
    @Column(name = "city", nullable = false)
    private String city;
}
