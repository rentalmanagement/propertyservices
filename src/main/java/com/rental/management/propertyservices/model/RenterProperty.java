package com.rental.management.propertyservices.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "renter_property")
public class RenterProperty {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "propertyid", nullable = false)
    private Long propertyid;
    @Column(name = "renterid", nullable = false)
    private Long renterid;

}
