package com.rental.management.propertyservices.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "prices")
public class Price {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "price", nullable = false)
    private BigDecimal price;
    @Column(name = "isPromotion", nullable = false)
    private boolean isPromotion;
    @Column(name = "discountRate", nullable = false)
    private Integer discountRate;
}
