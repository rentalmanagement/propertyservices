package com.rental.management.propertyservices.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "properties")
public class Property {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @OneToOne(
            cascade = CascadeType.ALL
    )
    private Address address;
    @Column(name = "number_of_rooms")
    private Integer numberOfRooms;
    @Column(name = "number_of_bathrooms")
    private Integer numberOfBathrooms;
    @Column(name = "area_size")
    private Integer areaSize;
    /*
    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    private Price price;
    */
    @Column(name = "price")
    private Integer price;
    @Column(name = "description")
    private String description;
    @Column(name="landlord_id")
    private Long landlordId;
}
