package com.rental.management.propertyservices.repository;

import com.rental.management.propertyservices.model.Property;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PropertyRepository extends JpaRepository<Property, Long> {
    List<Property> findPropertiesByLandlordId(Long id);
}
