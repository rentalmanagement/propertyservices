package com.rental.management.propertyservices.repository;

import com.rental.management.propertyservices.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AddressRepository extends JpaRepository<Address, Long> {
    List<Address> findAddressesByCityEquals(String city);
}
