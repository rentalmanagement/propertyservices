package com.rental.management.propertyservices.repository;

import com.rental.management.propertyservices.model.RenterProperty;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RenterPropertyRepository extends JpaRepository<RenterProperty, Long> {
    List<RenterProperty> findRenterPropertiesByPropertyid(Long id);
    RenterProperty findRenterPropertyByRenterid(Long renterid);
}
