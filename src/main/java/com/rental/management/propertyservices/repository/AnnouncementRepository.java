package com.rental.management.propertyservices.repository;

import com.rental.management.propertyservices.model.Announcement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AnnouncementRepository extends JpaRepository<Announcement, Long> {
    List<Announcement> findAnnouncementsByUserid(Long id);
}
