package com.rental.management.propertyservices.repository;

import com.rental.management.propertyservices.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PriceRepository extends JpaRepository<Price, Long> {

}
