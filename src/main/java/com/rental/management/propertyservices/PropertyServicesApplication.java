package com.rental.management.propertyservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.rental.management.propertyservices.repository")
@SpringBootApplication
public class PropertyServicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(PropertyServicesApplication.class, args);
    }
}
