package com.rental.management.propertyservices.config;


import com.rental.management.propertyservices.PropertyServicesApplication;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.HashSet;

@Configuration
@EnableSwagger2
public class SwaggerDocumentationConfig {
    @Value("${document.contact-email}")
    private String contactEmail;

    @Bean
    public Docket defaultApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("propertyServices")
                .select()
                .apis(RequestHandlerSelectors.basePackage(PropertyServicesApplication.class.getPackage().getName()))
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(false)
                .pathMapping("/")
                .produces(new HashSet<>(Collections.singletonList(MediaType.APPLICATION_JSON.toString())))
                .consumes(new HashSet<>(Collections.singletonList(MediaType.APPLICATION_JSON.toString())))
                .protocols(new HashSet<>(Collections.singletonList("https")))
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {

        ApiInfo apiInfo = new ApiInfo("PropertyServices REST Services",
                "Order REST Services for UserServices. ",
                "1.0",
                "",
                new Contact("IT", "", contactEmail),
                "",
                "");
        return apiInfo;
    }
}
