package com.rental.management.propertyservices.controller;


import com.rental.management.propertyservices.facade.PropertyFacade;
import com.rental.management.propertyservices.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/addresses")
public class AddressController {

    @Autowired
    private PropertyFacade propertyFacade;

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Address> getAll() {
        return propertyFacade.getAllAddresses();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Address getById(@PathVariable("id") Long id) {
        return propertyFacade.getAddressById(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Address> getByCity(@RequestParam("city") String city) {
        return propertyFacade.getAddressesByCity(city);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Address save(@RequestBody Address address) {
        return propertyFacade.saveAddress(address);
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id) {
        propertyFacade.deleteAddress(id);
    }


    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Address update(@RequestBody Address address) {
        return propertyFacade.updateAddress(address);
    }

}
