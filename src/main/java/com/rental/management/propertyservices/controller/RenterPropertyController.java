package com.rental.management.propertyservices.controller;

import com.rental.management.propertyservices.facade.PropertyFacade;
import com.rental.management.propertyservices.model.RenterProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/renter")
@CrossOrigin("*")
public class RenterPropertyController {
    @Autowired
    private PropertyFacade propertyFacade;

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<RenterProperty> getAll() {
        return propertyFacade.getAllRenterProperties();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public RenterProperty getById(@PathVariable("id") Long id) {
        return propertyFacade.getRenterPropertyById(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public RenterProperty getByRenterId(@RequestParam("renterid") Long id) {
        return propertyFacade.getRenterPropertyByRenterId(id);
    }

    @GetMapping("/property/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<RenterProperty> getByPropertyId(@PathVariable("id") Long id) {
        return propertyFacade.getAllRenterPropertiesByPropertyId(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RenterProperty save(@RequestBody RenterProperty renterProperty) {
        return propertyFacade.saveRenterProperty(renterProperty);
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id) {
        propertyFacade.deleteRenterProperty(id);
    }

}
