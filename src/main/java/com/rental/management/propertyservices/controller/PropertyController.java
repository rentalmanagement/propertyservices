package com.rental.management.propertyservices.controller;


import com.rental.management.propertyservices.facade.PropertyFacade;
import com.rental.management.propertyservices.model.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/properties")
public class PropertyController {

    @Autowired
    private PropertyFacade propertyFacade;

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin("*")
    public List<Property> getAll() {
        return propertyFacade.getAllProperties();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin("*")
    public Property getById(@PathVariable("id") Long id) {
        return propertyFacade.getPropertyById(id);
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin("*")
    public List<Property> getByLandlordId(@RequestParam("landlord") Long id) {
        return propertyFacade.getPropertiesByLandlordId(id);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @CrossOrigin("*")
    public Property save(@RequestBody Property property) {
        return propertyFacade.saveProperty(property);
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @CrossOrigin("*")
    public void delete(@PathVariable("id") Long id) {
        propertyFacade.deleteProperty(id);
    }


    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin("*")
    public Property update(@RequestBody Property property) {
        return propertyFacade.updateProperty(property);
    }

}
