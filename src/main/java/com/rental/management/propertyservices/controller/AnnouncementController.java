package com.rental.management.propertyservices.controller;

import com.rental.management.propertyservices.facade.PropertyFacade;
import com.rental.management.propertyservices.model.Announcement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/announcements")
public class AnnouncementController {
    @Autowired
    private PropertyFacade propertyFacade;

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin("*")
    public List<Announcement> getAll() {
        return propertyFacade.getAllAnnouncements();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin("*")
    public Announcement getById(@PathVariable("id") Long id) {
        return propertyFacade.getAnnouncementById(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin("*")
    public List<Announcement> getByUserId(@RequestParam("userid") Long userId) {
        return propertyFacade.getAnnouncementsByUserId(userId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @CrossOrigin("*")
    public Announcement save(@RequestBody Announcement announcement) {
        return propertyFacade.saveAnnouncement(announcement);
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @CrossOrigin("*")
    public void delete(@PathVariable("id") Long id) {
        propertyFacade.deleteAnnouncement(id);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin("*")
    public Announcement update(@RequestBody Announcement announcement) {
        return propertyFacade.updateAnnouncement(announcement);
    }
}
