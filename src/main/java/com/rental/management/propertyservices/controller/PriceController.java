package com.rental.management.propertyservices.controller;


import com.rental.management.propertyservices.facade.PropertyFacade;
import com.rental.management.propertyservices.model.Price;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/prices")
public class PriceController {

    @Autowired
    private PropertyFacade propertyFacade;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Price> getAll() {
        return propertyFacade.getAllPrices();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Price getById(@PathVariable("id") Long id) {
        return propertyFacade.getPriceById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Price save(@RequestBody Price price) {
        return propertyFacade.savePrice(price);
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id) {
        propertyFacade.deletePrice(id);
    }


    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Price update(@RequestBody Price price) {
        return propertyFacade.updatePrice(price);
    }

}
