package com.rental.management.propertyservices.service;

import com.rental.management.propertyservices.exception.ResourceNotFoundException;
import com.rental.management.propertyservices.model.RenterProperty;
import com.rental.management.propertyservices.repository.RenterPropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RenterPropertyService {
    @Autowired
    private RenterPropertyRepository renterPropertyRepository;

    @Transactional(readOnly = true)
    public RenterProperty getByRenterId(Long id) {
        return renterPropertyRepository.findRenterPropertyByRenterid(id);

    }
    @Transactional(readOnly = true)
    public RenterProperty getById(Long id) {
        return renterPropertyRepository.findById(id).orElseThrow(ResourceNotFoundException::new);

    }

    @Transactional(readOnly = true)
    public List<RenterProperty> getByPropertyId(Long idUser) {
        return renterPropertyRepository.findRenterPropertiesByPropertyid(idUser);
    }

    @Transactional(readOnly = true)
    public List<RenterProperty> getAll() {
        return renterPropertyRepository.findAll();
    }

    @Transactional
    public RenterProperty save(RenterProperty renterProperty) {
        return renterPropertyRepository.save(renterProperty);
    }

    @Transactional
    public void delete(Long id) {
        renterPropertyRepository.deleteById(id);
    }


}
