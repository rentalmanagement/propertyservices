package com.rental.management.propertyservices.service;

import com.rental.management.propertyservices.exception.ResourceNotFoundException;
import com.rental.management.propertyservices.model.Address;
import com.rental.management.propertyservices.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class AddressService {
    @Autowired
    private AddressRepository addressRepository;

    @Transactional(readOnly = true)
    public Address getById(Long id) {
        return addressRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Transactional(readOnly = true)
    public List<Address> getAll() {
        return addressRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Address> getByCity(String value) {
        return addressRepository.findAddressesByCityEquals(value.toLowerCase());
    }

    @Transactional
    public Address save(Address address) {
        return addressRepository.save(address);
    }

    @Transactional
    public Address update(Address address) {
        Address a = addressRepository.findById(address.getId()).orElseThrow(ResourceNotFoundException::new);

        a.setAddress(address.getAddress());
        a.setCity(address.getCity());
        a.setCounty(address.getCounty());

        return addressRepository.save(a);
    }

    @Transactional
    public void delete(Long id) {
        addressRepository.deleteById(id);
    }

}
