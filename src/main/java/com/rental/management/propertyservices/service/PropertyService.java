package com.rental.management.propertyservices.service;

import com.rental.management.propertyservices.exception.ResourceNotFoundException;
import com.rental.management.propertyservices.model.Property;
import com.rental.management.propertyservices.repository.AddressRepository;
import com.rental.management.propertyservices.repository.PriceRepository;
import com.rental.management.propertyservices.repository.PropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PropertyService {
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private PriceRepository priceRepository;
    @Autowired
    private PropertyRepository propertyRepository;

    @Transactional
    public Property save(Property property) {
        return propertyRepository.save(property);
    }

    @Transactional
    public void delete(Long id) {
        propertyRepository.deleteById(id);
    }

    @Transactional
    public Property update(Property property) {
        Property p = propertyRepository.findById(property.getId()).orElseThrow(ResourceNotFoundException::new);
        p.setAddress(property.getAddress());
        p.setAreaSize(property.getAreaSize());
        p.setDescription(property.getDescription());
        p.setNumberOfBathrooms(property.getNumberOfBathrooms());
        p.setNumberOfRooms(property.getNumberOfRooms());
        p.setPrice(property.getPrice());

        return propertyRepository.save(p);

    }

    @Transactional(readOnly = true)
    public Property getById(Long id) {
        return propertyRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Transactional(readOnly = true)
    public List<Property> getAll() {
        return propertyRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Property> getAllByLandlordId(Long id) {
        return propertyRepository.findPropertiesByLandlordId(id);
    }
}
