package com.rental.management.propertyservices.service;

import com.rental.management.propertyservices.exception.ResourceNotFoundException;
import com.rental.management.propertyservices.model.Announcement;
import com.rental.management.propertyservices.repository.AnnouncementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AnnouncementService {
    @Autowired
    private AnnouncementRepository announcementRepository;

    @Transactional(readOnly = true)
    public Announcement getById(Long id) {
        return announcementRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Transactional(readOnly = true)
    public List<Announcement> getByUser(Long idUser) {
        return announcementRepository.findAnnouncementsByUserid(idUser);
    }

    @Transactional(readOnly = true)
    public List<Announcement> getAll() {
        return announcementRepository.findAll();
    }

    @Transactional
    public Announcement save(Announcement announcement) {
        return announcementRepository.save(announcement);
    }

    @Transactional
    public void delete(Long id) {
        announcementRepository.deleteById(id);
    }

    @Transactional
    public Announcement update(Announcement announcement) {
        Announcement ann = announcementRepository.findById(announcement.getId()).orElseThrow(ResourceNotFoundException::new);

        ann.setTitle(announcement.getTitle());

        return announcementRepository.save(ann);
    }


}
