package com.rental.management.propertyservices.service;

import com.rental.management.propertyservices.exception.ResourceNotFoundException;
import com.rental.management.propertyservices.model.Price;
import com.rental.management.propertyservices.repository.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class PriceService {
    @Autowired
    private PriceRepository priceRepository;

    @Transactional(readOnly = true)
    public List<Price> getAll() {
        return priceRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Price getById(Long id) {
        return priceRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    @Transactional
    public Price save(Price price) {
        return priceRepository.save(price);
    }

    @Transactional
    public void delete(Long id) {
        priceRepository.deleteById(id);
    }

    @Transactional
    public Price update(Price price) {
        Price p = priceRepository.findById(price.getId()).orElseThrow(ResourceNotFoundException::new);

        p.setDiscountRate(price.getDiscountRate());
        p.setPrice(price.getPrice());
        p.setPromotion(price.isPromotion());

        return priceRepository.save(p);
    }


}
